package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;
import com.qaagility.controller.Counter;

public class CounterTest {
    @Test
    public void testDZero() throws Exception {
        final int resp = new Counter().divide(1, 0);
        assertEquals("resp", Integer.MAX_VALUE, resp);
    }

    @Test
    public void testD() throws Exception {
        final int resp = new Counter().divide(1, 1);
        assertEquals("resp", 1, resp);
    }
}
