package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;
import com.qaagility.controller.Calculator;

public class CalculatorTest {
    @Test
    public void testAdd() throws Exception {

        final int resp = new Calculator().add();
        assertEquals("resp", 9, resp);
    }
}
