package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;
import com.qaagility.controller.About;

public class AboutTest {
    @Test
    public void testDesc() throws Exception {

        final String resp = new About().desc();
        assertEquals("resp", "This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!", resp);
    }
}
