package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;
import com.qaagility.controller.Calcmul;

public class CalcmulTest {
    @Test
    public void testMul() throws Exception {

        final int resp = new Calcmul().mul();
        assertEquals("resp", 18, resp);
    }
}
